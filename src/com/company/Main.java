package com.company;

import java.util.GregorianCalendar;
import java.util.*;

public class Main {

    public static void main(String[] args) {
	Pracownik[] obsluga = new Pracownik[3];

	obsluga[0]=new Pracownik("Karol Kraker", 75000, 1987, 12, 15);
	obsluga[1]=new Pracownik("Henryk Hacker",50000,1989,10,1);
	obsluga[2]=new Pracownik("Tomasz Tester",40000,1990,3,15);

	for(int i=0; i<obsluga.length; i++){
	    obsluga[i].podniesPensje(5);
    }

    for(int i=0; i<obsluga.length; i++){
        Pracownik p=obsluga[i];
        System.out.println("nazwisko = "+p.pobierzNazw()+", pensja = "+p.podniesPensje() +", dzień zatrudnienia = "+p.pobierzDzienZatrunienia());
    }

    }

    class Pracownik{
        public Pracownik(String n, double p, int rok, int miesiac, int dzien){
            nazwisko = n;
            pensja = p;
            GregorianCalendar kalendarz = new GregorianCalendar(rok, miesiac-1, dzien);//greg.cal. liczy miesiace od 0
            dzienZatrudnienia = kalendarz.getTime();
        }

        public String pobierzNazw(){
            return nazwisko;
        }

        public double pobierzPensje(){
            return pensja;
        }

        public Date pobierzDzienZatrunienia(){
            return dzienZatrudnienia;
        }

        public void podniesPensje(double oProcent){
            double podwyzka = pensja*oProcent/100;
            pensja+=podwyzka;
        }

        private String nazwisko;
        private double pensja;
        private Date dzienZatrudnienia;
    }
}
